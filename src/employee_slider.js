import React, { Component } from 'react';


class EmployeeSlider extends Component {
  render() {
	  
    return (
      <div className="emp-wrap clearfix" id="emp-slider-outer">
			<div className="desktop" id="emp-slider">
				<ul className="emp-slider desktop" id="emp-slider-count" style={{left:0,top:0}} >
					<li >
						<div className="emp-header">Employee Outcomes</div>
						<div className="emp-block">
							<div className="emp-count">70<sup>%</sup></div>
							<div className="emp-count-desc">of members raise their <br />credit score by 30+ points</div>
						</div>
						<div className="emp-block">
							<div className="emp-count"><sup>$</sup>300</div> 
							<div className="emp-count-desc">average investment <br />contribution in the first <br />six months</div>
						</div>
						<div className="emp-block">
							<div className="emp-count">60<sup>%</sup></div>
							<div className="emp-count-desc">of members improve <br />their financial literacy <br />by logging in daily</div>
						</div>
						<div className="emp-block">
							<div className="emp-count"><sup>$</sup>329</div>
							<div className="emp-count-desc">in bank fees eliminated <br />on average</div>
						</div>
					</li>
					<li >
						<div className="emp-header">Employer Outcomes</div>
						<div className="emp-block">
							<div className="emp-count">85<sup>%</sup></div>
							<div className="emp-count-desc">increase <br />in motivation</div>
						</div>
						<div className="emp-block">
							<div className="emp-count">75<sup>%</sup></div>
							<div className="emp-count-desc">fewer unplanned <br />absences</div>
						</div>
						<div className="emp-block">
							<div className="emp-count">20</div>
							<div className="emp-count-desc">hours average monthly <br />productivity gain, per <br />employee</div>
						</div>
						<div className="emp-block">
							<div className="emp-count"><sup>$</sup>MM</div>
							<div className="emp-count-desc">of value from <br />productivity gains</div>
						</div>
					</li>
				</ul>
			</div>				
			<ul className="emp-slider mobile">
            	<li className="clearfix" >
                	<div className="emp-header">Employee Outcomes</div>
                    <div className="emp-block">
                    	<div className="emp-count">70<sup>%</sup></div>
                        <div className="emp-count-desc">of members raise their <br />credit score by 30+ points</div>
                    </div>
                    <div className="emp-block">
                    	<div className="emp-count"><sup>$</sup>300</div> 
                        <div className="emp-count-desc">average investment <br />contribution in the first <br />six months</div>
                    </div>
                    <div className="emp-block">
                    	<div className="emp-count">60<sup>%</sup></div>
                        <div className="emp-count-desc">of members improve <br />their financial literacy <br />by logging in daily</div>
                    </div>
                    <div className="emp-block">
                    	<div className="emp-count"><sup>$</sup>329</div>
                        <div className="emp-count-desc">in bank fees eliminated <br />on average</div>
                    </div>
                </li>
                <li className="second-child clearfix">
                	<div className="emp-header">Employer Outcomes</div>
                    <div className="emp-block">
                    	<div className="emp-count">85<sup>%</sup></div>
                        <div className="emp-count-desc">increase <br />in motivation</div>
                    </div>
                    <div className="emp-block">
                    	<div className="emp-count">75<sup>%</sup></div>
                        <div className="emp-count-desc">fewer unplanned <br />absences</div>
                    </div>
                    <div className="emp-block">
                    	<div className="emp-count">20</div>
                        <div className="emp-count-desc">hours average monthly <br />productivity gain, per <br />employee</div>
                    </div>
                    <div className="emp-block">
                    	<div className="emp-count"><sup>$</sup>MM</div>
                        <div className="emp-count-desc">of value from <br />productivity gains</div>
                    </div>
                </li>
            </ul>
        </div>
    );
  }
}


export default EmployeeSlider;
