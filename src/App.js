import React, { Component } from 'react';
import './App.css';

import Header from './header';
import Footer from './footer';
import TopBanner from './topbanner';
import BusinessSlider from './business_slider';
import FinancialSlider from './financial_slider';
import BonusWrap from './bonus_wrap';
import EmployeeSlider from './employee_slider';
import GettingWrap from './getting_wrap';
import FormsWrap from './forms_wrap';


class App extends Component {
  render() {
    return (
      <div>
        <Header />
		<TopBanner />
		<BusinessSlider />
		<FinancialSlider />
		<BonusWrap />
		<EmployeeSlider />
		<GettingWrap />
		<FormsWrap />
		<Footer />
	  </div>
    );
  }
  componentDidMount() {
	var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
    if(navigator.userAgent.indexOf('Mac') > 0){
		document.body.classList.add('mac-os');
	}
	
	//var businessY = 0;
	var financialY = 0;
	var empY = 0;
	var empMax = 0;

	// GET OFFSET POSITION
	function getOffset( el ) {
		var _x = 0;
		var _y = 0;
		while( el && !isNaN( el.offsetLeft ) && !isNaN( el.offsetTop ) ) {
			_x += el.offsetLeft - el.scrollLeft;
			_y += el.offsetTop - el.scrollTop;
			el = el.offsetParent;
		}
		return { top: _y, left: _x };
	}
	//var wHeight = window.innerHeight;
	setTimeout(function() {
		//businessY = Math.round(getOffset( document.getElementById('business-slider') ).top-((window.innerHeight-600)/2));
		financialY = Math.round(getOffset( document.getElementById('financial-slider-outer') ).top-((window.innerHeight-600)/2)-47);
		empY = Math.round(getOffset( document.getElementById('emp-slider-outer') ).top-((window.innerHeight-450)/2)-47);
		
		empMax = (document.getElementById('emp-slider-count').offsetHeight/2)+25;
	}, 1000);
	window.addEventListener("orientationchange", function() {
    	//businessY = Math.round(getOffset( document.getElementById('business-slider') ).top-((window.innerHeight-600)/2));
		financialY = Math.round(getOffset( document.getElementById('financial-slider-outer') ).top-((window.innerHeight-600)/2)-47);
		empY = Math.round(getOffset( document.getElementById('emp-slider-outer') ).top-((window.innerHeight-450)/2)-47);
		
		empMax = (document.getElementById('emp-slider-count').offsetHeight/2)+25;
	});
	document.addEventListener('keydown', (event) => {
	  var keyName = event.keyCode;
	  console.log(keyName);
	  if(keyName === 40){
		 windowScroll = true;
	  }
	  if(keyName === 38){
		 windowScroll = false;
	  }
	});
	
	window.addEventListener('scroll', function ( event ) {
		if(!isSafari){
			financialY = Math.round(getOffset( document.getElementById('financial-slider-outer') ).top-((window.innerHeight-600)/2)-47);
			empY = Math.round(getOffset( document.getElementById('emp-slider-outer') ).top-((window.innerHeight-450)/2)-47);
			
			empMax = (document.getElementById('emp-slider-count').offsetHeight/2)+25;
		}
			if(window.scrollY <= financialY){
				/*document.getElementById('financial-slider-count').getElementsByTagName('li')[0].style.opacity = '1';
				document.getElementById('financial-slider-count').getElementsByTagName('li')[1].style.opacity = '0';
				document.getElementById('financial-slider-count').getElementsByTagName('li')[2].style.opacity = '0';*/
				document.getElementById('financial-slider-count').getElementsByTagName('li')[0].classList.add('active');
				document.getElementById('financial-slider-count').getElementsByTagName('li')[1].classList.remove('remove');
				document.getElementById('financial-slider-count').getElementsByTagName('li')[2].classList.remove('remove');
			}
			if(window.scrollY-financialY >= 1 && window.scrollY-financialY <= 30){
				/*document.getElementById('financial-slider-count').getElementsByTagName('li')[0].style.opacity = '1';
				document.getElementById('financial-slider-count').getElementsByTagName('li')[1].style.opacity = '0';
				document.getElementById('financial-slider-count').getElementsByTagName('li')[2].style.opacity = '0';*/
				document.getElementById('financial-slider-count').getElementsByTagName('li')[0].classList.add('active');
				document.getElementById('financial-slider-count').getElementsByTagName('li')[1].classList.remove('active');
				document.getElementById('financial-slider-count').getElementsByTagName('li')[2].classList.remove('active');
			}
			if(window.scrollY-financialY >= 31 && window.scrollY-financialY <= 250){
				/*document.getElementById('financial-slider-count').getElementsByTagName('li')[0].style.opacity = '0';
				document.getElementById('financial-slider-count').getElementsByTagName('li')[1].style.opacity = '1';
				document.getElementById('financial-slider-count').getElementsByTagName('li')[2].style.opacity = '0';*/
				document.getElementById('financial-slider-count').getElementsByTagName('li')[0].classList.remove('active');
				document.getElementById('financial-slider-count').getElementsByTagName('li')[1].classList.add('active');
				document.getElementById('financial-slider-count').getElementsByTagName('li')[2].classList.remove('active');	
			}
			if(window.scrollY-financialY >= 251 && window.scrollY-financialY <= 300){
				/*document.getElementById('financial-slider-count').getElementsByTagName('li')[0].style.opacity = '0';
				document.getElementById('financial-slider-count').getElementsByTagName('li')[1].style.opacity = '0';
				document.getElementById('financial-slider-count').getElementsByTagName('li')[2].style.opacity = '1';*/
				document.getElementById('financial-slider-count').getElementsByTagName('li')[0].classList.remove('active');
				document.getElementById('financial-slider-count').getElementsByTagName('li')[1].classList.remove('active');
				document.getElementById('financial-slider-count').getElementsByTagName('li')[2].classList.add('active');	
			}
			if(window.scrollY-financialY >= 301){
				/*document.getElementById('financial-slider-count').getElementsByTagName('li')[0].style.opacity = '0';
				document.getElementById('financial-slider-count').getElementsByTagName('li')[1].style.opacity = '0';
				document.getElementById('financial-slider-count').getElementsByTagName('li')[2].style.opacity = '1';	*/
				document.getElementById('financial-slider-count').getElementsByTagName('li')[0].classList.remove('active');
				document.getElementById('financial-slider-count').getElementsByTagName('li')[1].classList.remove('active');
				document.getElementById('financial-slider-count').getElementsByTagName('li')[2].classList.add('active');	
			}
			/*if(window.scrollY <= financialY){
				document.getElementById('financial-slider-count').getElementsByTagName('li')[0].style.opacity = '1';	
			}
			if(window.scrollY-financialY >= 1 && window.scrollY-financialY <= 10){
				document.getElementById('financial-slider-count').getElementsByTagName('li')[0].style.opacity = '0.8';	
			}
			if(window.scrollY-financialY >= 11 && window.scrollY-financialY <= 20){
				document.getElementById('financial-slider-count').getElementsByTagName('li')[0].style.opacity = '0.6';	
			}
			if(window.scrollY-financialY >= 21 && window.scrollY-financialY <= 30){
				document.getElementById('financial-slider-count').getElementsByTagName('li')[0].style.opacity = '0.4';	
			}
			if(window.scrollY-financialY >= 31 && window.scrollY-financialY <= 40){
				document.getElementById('financial-slider-count').getElementsByTagName('li')[0].style.opacity = '0.2';	
			}
			if(window.scrollY-financialY >= 41 && window.scrollY-financialY <= 50){
				document.getElementById('financial-slider-count').getElementsByTagName('li')[0].style.opacity = '0';	
			}
			if(window.scrollY-financialY >= 51 ){
				document.getElementById('financial-slider-count').getElementsByTagName('li')[0].style.opacity = '0';	
			}
			
			if(window.scrollY-financialY <= 51 ){
				document.getElementById('financial-slider-count').getElementsByTagName('li')[1].style.opacity = '0';	
			}
			if(window.scrollY-financialY >= 51 && window.scrollY-financialY <= 60){
				document.getElementById('financial-slider-count').getElementsByTagName('li')[1].style.opacity = '0.2';	
			}
			if(window.scrollY-financialY >= 61 && window.scrollY-financialY <= 70){
				document.getElementById('financial-slider-count').getElementsByTagName('li')[1].style.opacity = '0.4';	
			}
			if(window.scrollY-financialY >= 71 && window.scrollY-financialY <= 80){
				document.getElementById('financial-slider-count').getElementsByTagName('li')[1].style.opacity = '0.6';	
			}
			if(window.scrollY-financialY >= 81 && window.scrollY-financialY <= 90){
				document.getElementById('financial-slider-count').getElementsByTagName('li')[1].style.opacity = '0.8';	
			}
			if(window.scrollY-financialY >= 91 && window.scrollY-financialY <= 100){
				document.getElementById('financial-slider-count').getElementsByTagName('li')[1].style.opacity = '1';	
			}
			if(window.scrollY-financialY >= 101 && window.scrollY-financialY <= 110){
				document.getElementById('financial-slider-count').getElementsByTagName('li')[1].style.opacity = '0.8';	
			}
			if(window.scrollY-financialY >= 111 && window.scrollY-financialY <= 120){
				document.getElementById('financial-slider-count').getElementsByTagName('li')[1].style.opacity = '0.6';	
			}
			if(window.scrollY-financialY >= 121 && window.scrollY-financialY <= 130){
				document.getElementById('financial-slider-count').getElementsByTagName('li')[1].style.opacity = '0.4';	
			}
			if(window.scrollY-financialY >= 131 && window.scrollY-financialY <= 140){
				document.getElementById('financial-slider-count').getElementsByTagName('li')[1].style.opacity = '0.2';	
			}
			if(window.scrollY-financialY >= 141 && window.scrollY-financialY <= 150){
				document.getElementById('financial-slider-count').getElementsByTagName('li')[1].style.opacity = '0';	
			}
			if(window.scrollY-financialY >= 151 ){
				document.getElementById('financial-slider-count').getElementsByTagName('li')[1].style.opacity = '0';	
			}
			
			if(window.scrollY-financialY <= 151 ){
				document.getElementById('financial-slider-count').getElementsByTagName('li')[2].style.opacity = '0';	
			}
			if(window.scrollY-financialY >= 151 && window.scrollY-financialY <= 160){
				document.getElementById('financial-slider-count').getElementsByTagName('li')[2].style.opacity = '0.2';	
			}
			if(window.scrollY-financialY >= 161 && window.scrollY-financialY <= 170){
				document.getElementById('financial-slider-count').getElementsByTagName('li')[2].style.opacity = '0.4';	
			}
			if(window.scrollY-financialY >= 171 && window.scrollY-financialY <= 180){
				document.getElementById('financial-slider-count').getElementsByTagName('li')[2].style.opacity = '0.6';	
			}
			if(window.scrollY-financialY >= 181 && window.scrollY-financialY <= 190){
				document.getElementById('financial-slider-count').getElementsByTagName('li')[2].style.opacity = '0.8';	
			}
			if(window.scrollY-financialY >= 191 && window.scrollY-financialY <= 200){
				document.getElementById('financial-slider-count').getElementsByTagName('li')[2].style.opacity = '1';	
			}
			if(window.scrollY-financialY >= 200 ){
				document.getElementById('financial-slider-count').getElementsByTagName('li')[2].style.opacity = '1';	
			}*/
			
		//console.log(window.scrollY+'       '+windowScroll);
		/*if(window.innerWidth > 1024){ 
			if(window.scrollY >= 35){
				document.getElementById('root').classList.add('frezz');
			}else {
				document.getElementById('root').classList.remove('frezz');
			}
		}*/
		
		
		if(window.innerWidth >= 1024){
			if(windowScroll){
				/*if(window.scrollY >= financialY){
					if(parseInt(document.getElementById('financial-slider-count').style.left) > -640){
						window.scrollTo(0,financialY);
						
						document.getElementById('financial-slider-count').style.left = (parseInt(document.getElementById('financial-slider-count').style.left)-10)+"px";
						if(parseInt(document.getElementById('financial-slider-count').style.left) < -640){
							document.getElementById('financial-slider-count').style.left = -640+"px";
						}
						
					}
				}*/
				
				if(window.scrollY >= empY){
					if(parseInt(document.getElementById('emp-slider-count').style.top) > -(empMax)){
						window.scrollTo(0,empY);
						document.getElementById('emp-slider-count').style.top = (parseInt(document.getElementById('emp-slider-count').style.top)-20)+"px";
						if(parseInt(document.getElementById('emp-slider-count').style.top) < -(empMax)){
							document.getElementById('emp-slider-count').style.top = -(empMax)+"px";
						}
					}
				}
			}else {
				/*if(window.scrollY <= financialY){
					if(parseInt(document.getElementById('financial-slider-count').style.left) < 0){
						window.scrollTo(0,financialY);
						document.getElementById('financial-slider-count').style.left = (parseInt(document.getElementById('financial-slider-count').style.left)+10)+"px";
						if(parseInt(document.getElementById('financial-slider-count').style.left) > 0){
							document.getElementById('financial-slider-count').style.left = 0+"px";
						}
					}
				}*/
				
				if(window.scrollY <= empY){
					if(parseInt(document.getElementById('emp-slider-count').style.top) < 0){
						window.scrollTo(0,empY);
						document.getElementById('emp-slider-count').style.top = (parseInt(document.getElementById('emp-slider-count').style.top)+20)+"px";
						if(parseInt(document.getElementById('emp-slider-count').style.top) > 0){
							document.getElementById('emp-slider-count').style.top = 0+"px";
						}
					}
				}
			}
		}
		
	});
	var windowScroll = false;
	var scrollableElement = window;
	
	scrollableElement.addEventListener('wheel', findScrollDirectionOtherBrowsers);
	
	function findScrollDirectionOtherBrowsers(event){
		var delta;
		
		if (event.wheelDelta){
			delta = event.wheelDelta;
		}else{
			delta = -1 * event.deltaY;
		}
		
		if (delta < 0){
			//console.log("DOWN");
			windowScroll = true;
			
		
		}else if (delta > 0){
			windowScroll = false;
			//console.log("UP");
			
		}
		
	}
	
	

  }
}


export default App;

